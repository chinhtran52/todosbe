# TodosBE 
# Khac Chinh + Quoc Bao

API todosbe docs

# Root 
https://todosbe.herokuapp.com/

# Login 
0. Get userID
- Endpoint: /login
- Method: POST
- Body JSON: email, password

# Users 

1. Get all users:
- Endpoint: /users
- Method: GET

2. Get detail user:
- Endpoint: /users/:userID
- Method: GET

3. Add new user:
- Endpoint: /users
- Method: POST
- Body JSON: email, password, username(not require)
* note: Default role is user when creating new user

4. Delete user:
- Endpoint: /users/:userID
- Method: DELETE

5. Update user:
- Endpoint: /users/:userID
- Method: PUT
- Body JSON: email, password, role, username

# Tasks

6. Get all tasks:
- Endpoint: /tasks
- Method: GET

7. Get detail task:
- Endpoint: /tasks/:taskID
- Method: GET

8. Add new task:
- Endpoint: /tasks
- Method: POST
- Body JSON: title, uid, date

9. Delete task:
- Endpoint: /tasks/:taskID
- Method: DELETE

10. Update task:
- Endpoint: /tasks/:taskID
- Method: PUT
- Body JSON: email, password, role, username

11. Get all tasks of a user
- Endpoint: /:userID/tasks
- Method: GET

12. Get all daily tasks of a user
- Endpoint: /:userID/tasks/:date
- Method: GET
*note: date format: ddMMyy - sample: '23-12-2019'
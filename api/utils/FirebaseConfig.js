
const admin = require("firebase-admin");

const serviceAccount = require("../../ServiceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://calendarapp-b50f8.firebaseio.com"
});

const firestore = admin.firestore()
const auth = admin.auth()

module.exports = {
  firestore : firestore,
  auth : auth
}
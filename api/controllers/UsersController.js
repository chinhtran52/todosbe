'use strict'

const db = require('../utils/FirebaseConfig')

module.exports = {
    get: (req, res) => {
        //Firestore
        let users = []
        db.firestore.collection('users').get().then((snapshot)=>{
            snapshot.forEach(item => {
                users.push({
                    uid : item.id,
                    username : item.data().username,
                    email : item.data().email,
                    password : item.data().password,
                    role : item.data().role
                })
              });
            res.json(users)
        })
    },
    detail: (req, res) => {
        db.firestore.collection('users').doc(req.params.taskID).get().then((snapshot)=>{
            res.json(snapshot.data())
        })
    },
    update: (req, res) => {
        const data = req.body;
        const taskID = req.params.taskID;
        db.firestore.collection('users').doc(taskID).update(data).then(
            res.json({message:"Success"})
        ).catch((error)=>{
            res.json({message:"Failed"})
        })
    },
    store: (req, res) => {
        const data = req.body;
        db.firestore.collection('users').doc().set({
            email : data.email,
            password : data.password,
            username : data.username?data.username:null,
            role : 'user'
        })
        .then(
            res.json({message:'Add success'})
        ).catch(
            res.status(404),
            res.json({message:'Add failed'})
        )
    },
    delete: (req, res) => {
        db.firestore.collection('users').doc(req.params.taskID).delete().then(
            res.json({message:'Delete task successfully'})
        )
    }
}
'use strict'

const db = require('../utils/FirebaseConfig')

module.exports = {
    get: (req, res) => {
        //// Realtime Database - need to change at FirebaseConfig
        // let results = [] 
        // db.ref('tasks').on('child_added',(snapshot)=>{
        //     console.log(snapshot.val())
        //     results.push({
        //         status: snapshot.val().status,
        //         title : snapshot.val().title,
        //         date : snapshot.val().date,
        //         uid: snapshot.val().uid
        //     })    
        // res.json(results)
        // })

        //Firestore
        let tasks = []
        db.firestore.collection('tasks').get().then((snapshot)=>{
            snapshot.forEach(item => {
                tasks.push({
                    uid : item.data().uid,
                    title : item.data().title,
                    date : item.data().date,
                    status : item.data().status,
                    id : item.id
                })
              });
            res.json(tasks)
        })
    },
    detail: (req, res) => {
        db.firestore.collection('tasks').doc(req.params.taskID).get().then((snapshot)=>{
            res.json(snapshot.data())
        })
    },
    update: (req, res) => {
        const data = req.body;
        const taskID = req.params.taskID;
        db.firestore.collection('tasks').doc(taskID).update(data).then(
            res.json({message:"Success"})
        ).catch((error)=>{
            res.json({message:"Failed"})
        })
    },
    store: (req, res) => {
        const data = req.body;
        db.firestore.collection('tasks').doc().set({
            title : data.title,
            uid : data.uid,
            status: false,
            date : data.date
        })
        .then(
            res.json({message:'Add success'})
        ).catch(
            res.json({message:'Add failed'})
        )
    },
    delete: (req, res) => {
        db.firestore.collection('tasks').doc(req.params.taskID).delete().then(
            res.json({message:'Delete task successfully'})
        )
    },
    getTasksFromUID: (req,res) => {
        let tasks = []
        db.firestore.collection('tasks').get().then((snapshot)=>{
            snapshot.forEach(item => {
                if(item.data().uid==req.params.userID) tasks.push({
                    title : item.data().title,
                    date : item.data().date,
                    uid : item.data().uid,
                    status : item.data().status,
                    id : item.id
                })
              });
            res.json(tasks)
        })
    },
    getTasksDate: (req,res) => {
        let tasks = []
        db.firestore.collection('tasks').get().then((snapshot)=>{
            snapshot.forEach(item => {
                if(item.data().uid==req.params.userID&&item.data().date==req.params.date) tasks.push({
                    uid : item.data().uid,
                    title : item.data().title,
                    date : item.data().date,
                    status : item.data().status,
                    id : item.id
                })
              });
            res.json(tasks)
        })
    },
}
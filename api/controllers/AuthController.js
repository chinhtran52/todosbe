'use strict'

const db = require('../utils/FirebaseConfig')

module.exports = {
    login: (req, res) => {
        //Firestore
        const data = {
            email : req.body.email,
            password : req.body.password
        }
        db.firestore.collection('users').get().then((snapshot)=>{
            let results;
            snapshot.forEach(item => {
                if((item.data().email==data.email)&&(item.data().password==data.password)){
                    results = 1;
                    res.json({
                        uid: item.id,
                        username: item.data().username,
                        password: item.data().password,
                        role: item.data().role,
                        email: item.data().email
                    }) 
                }                    
            });
            if (results == null){
                res.status(404);
                res.json({
                    message: "Sai email hoặc mật khẩu"
                })
            }
        })
    },
}
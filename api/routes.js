'use strict';
module.exports = function(app) {
  const tasksCtrl = require('./controllers/TasksController');
  const usersCtrl = require('./controllers/UsersController');
  const authCtrl = require('./controllers/AuthController');

  // todoList Routes
  app.route('/tasks')
    .get(tasksCtrl.get)
    .post(tasksCtrl.store);

  app.route('/:userID/tasks')
    .get(tasksCtrl.getTasksFromUID);

  app.route('/:userID/tasks/:date')
    .get(tasksCtrl.getTasksDate)

  app.route('/tasks/:taskID')
    .get(tasksCtrl.detail)
    .put(tasksCtrl.update)
    .delete(tasksCtrl.delete);

  app.route('/users')
    .get(usersCtrl.get)
    .post(usersCtrl.store);

  app.route('/users/:userID')
    .get(usersCtrl.detail)
    .put(usersCtrl.update)
    .delete(usersCtrl.delete);
  
  app.route('/login')
    .post(authCtrl.login)

  app.route('/signup')
    .post(usersCtrl.store)
};